---
layout: markdown_page
title: "Category Vision - Container Registry"
---


## Container Registry

GitLab Container Registry is a secure and private registry for Docker images. Built on open source software, GitLab Container Registry isn't just a standalone registry; it's completely integrated with GitLab.  Easily use your images for GitLab CI, create images specific for tags or branches and much more.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Container%20Registry)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1287) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

Next we will be prioritizing features [gitlab-ce#20247](https://gitlab.com/gitlab-org/gitlab-ce/issues/20247) and [gitlab-ce#24841](https://gitlab.com/gitlab-org/gitlab-ce/issues/24841) to help our customers optimize storage and save money by setting retention and expiration policies as well as establishing storage limits for the Container Registry. 

There are also a variety of requests to improve authentication. [gitlab-org#1243](https://gitlab.com/groups/gitlab-org/-/epics/1243) lists the autentication and permissions issues which we will be addressing in the coming months. 

## Competitive Landscape
Universal Package Managers such as [JFrog's Artifactory](https://jfrog.com/artifactory/) and [Sonatype's Nexus](https://www.sonatype.com/nexus-repository-sonatype) allow users to integrate with many different package management solutions, including Docker. They provide improved performance by allowing users to store and cache their binaries and avoid repetitive downloading with each build. Additionally, they provide advanced features such as vulnerability scanning and auditing. Container Package Managers such as [Docker Hub](https://hub.docker.com/) and [Quay](https://quay.io/) offer users a single location to build, analyze and distribute their container registries. 

GitLab provides an improved experience by being the single location for the entire DevOps Lifecycle, not just a portion of it. We will provide many of the features expected of a Package Management tool, but without the weight and complexity of a single-point solution. We will prioritize security, performance and integration without sacrificing user experience. 


## Top Customer Success/Sales Issue(s)

The top Customer Success / Sales issue is to improve the visibility and management layer of the Container Registry. The goal of [ce-#29639](https://gitlab.com/gitlab-org/gitlab-ce/issues/29639) is to improve the tracking and display of data to provide a more seamless user experience within GitLab. By completing this issue we will:
-  Allow  metadata to be stored and removed
-  Make it possible to easily track what data is stored in the registry
-  Make it possible to introduce retention policies for images stored in the registry


## Top Customer Issue(s)

There are two types of customer requests we see most often. The first is authentication. We've seen several issues from users who are having problems authenticating to Docker. The second is related to storage optimization. We recently released [ce-#25322](https://gitlab.com/gitlab-org/gitlab-ce/issues/25322) and [ce-#20170](https://gitlab.com/gitlab-org/gitlab-ce/issues/20170) to help users remove old images and run garbage collection. In the future, we will allow our customers to set storage limits and policies for their Container Registries.

## Top Internal Customer Issue(s)

The top internal customer issue is also tied to storage optimization. [ce-#57897](https://gitlab.com/gitlab-org/gitlab-ce/issues/57897) will allow the Infrastructure team to lower the total cost of the GitLab.com Container Registry by implementing in-line garbage collection and removal of Blobs. 

## Top Vision Item(s)

As we see increased adoption and usage of the Container Registry, the need for an improved user interface becomes more important. [ee-#3597](https://gitlab.com/gitlab-org/gitlab-ee/issues/3597) establishes a high-level vision for the future user experience of the Container Registry. In the coming months, we will break this feature into actionable issues and conduct user research to ensure we provide the best experience possible. 
