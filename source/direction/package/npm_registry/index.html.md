---
layout: markdown_page
title: "Category Vision - NPM Registry"
---

- TOC
{:toc}

## NPM Registry

JavaScript developers need a secure, standardized way to share and version control NPM packages across projects. This category is still at the "minimal" stage of maturity. In order to reach "complete" we must continue to expand the functionality and drive adoption. In the coming months, we will be adding templates for auto-deploying NPM packages and resolving issues with authentication.

As we evaluate additional package manager integrations, we will look to standardize our API, user permissions and the user interface. Your feedback will help us to prioritize and set those standards.  

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=NPM%20Registry)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1292) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

Next we will be prioritizing [gitlab-ee#10050](https://gitlab.com/gitlab-org/gitlab-ee/issues/10050) to provide users with a template for deploying packages to the GitLab NPM Registry. Delivering this issue will make it easier to adopt the NPM registry, increasing user adoption and usability of the feature. 

## Competitive Landscape

- [JFrog Artifactory](https://www.jfrog.com/confluence/display/RTF/Npm+Registry)
- [Sonatype Nexus](https://www.sonatype.com/nexus-repository-sonatype)


## Top Customer Success/Sales Issue(s)

The top customer success and sales issues are related to improving authentication for NPM. [gitlab-ee#9104](https://gitlab.com/gitlab-org/gitlab-ee/issues/9104) has been requested by several customers looking to use the CI_JOB_TOKEN to help provide a more seamless CI/CD experience, and will will standardize authentication across integrations to include personal access tokens. 

## Top Customer Issue(s)

The top customer issue is [gitlab-ee#9960](https://gitlab.com/gitlab-org/gitlab-ee/issues/9960) which is currently preventing users from publishing from within a subgroup. 

## Top Internal Customer Issue(s)

The top internal customer issue is tied to storage optimization. The first step is to understand the current and historical usage of the registry. [gitlab-ce#49412](https://gitlab.com/gitlab-org/gitlab-ce/issues/49412) will help us to measure historical usage and prioritize future improvements. 


## Top Vision Item(s)

The top vision item is [gitlab-ee#9164](https://gitlab.com/gitlab-org/gitlab-ee/issues/9164), which will add support for the dependency proxy to the NPM registry. Once launched, it will introduce security scanning and vulnerability testing.
